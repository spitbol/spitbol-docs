## Documentation

http://github.com/spitbol/spitbol-docs

This directory contains the documentation files for Macro SPITBOL, reference manuals for some of the architectures for which SPITBOL is available, and some of the tools used to build SPITBOL.

All files herein at coopyrighted and so not be altered in any way without permission.


### [The SNOBOL4 Programming Language](docs/green-book.pdf) by R. E. Griswold, J. F. Poage and I. P. Polonski.

This is the classic "Green Book" (because of the cover of its color). It is a marvel of typography, and first
introduced the use of "beads" to explain SNOBOL pattern matching.



### [MACRO SPITBOL: The High-Performance SNOBOL4 Language](docs/spitbol-manual-v3.7.pdf) by Mark Emmer and Edward Quillen.

This basic referenec for Macro SPITBOL was co-written by Mark Emmer and Edward Quillen.  Mark maintaine SPITBOL for well over twenty years, from the mid-1980's to 2009.

The SPITBOL project notes with sadness the death of Ed Quillen in June, 2012.
To quote from Mark Emmer's Acknowledgments in the SPITBOL Manual:

> Ed Quillen, local novelist, political columnist, and SNOBOL enthusiast, co-
authored this manual. He combined various terse SPITBOL documents from other
systems with Catspaw's SNOBOL4+ manual, while providing more complete
explanations of some concepts. Any observed clarity is this manual is due to
his contributions.

### v37.min

Macro SPITBOLis written in an abstract assembly language called MINIMAL. It is carefully designed so it can be translated to very efficient assembly code making full use of the target architrecture with minimal assumptions about the nature of the target architecture.  At a surface level is has a similarity to PDP-11 assembly language.

`v37.min` is the source file for SPITBOL v3.7. It is the state of SPITBOL as of 2009 when
Dave Shields took over the maintainer role. The actual code in the current system is
very close to v3.7. The main difference is that the change history has been deleted, and
the MINIMAL specification formerly in this document can now be found in the file `minimal.md`

The supplied version of v37.min is just the source for SPITBOL converted from all upper case
to all lower case.


### minimal.md

`minimal.md` contains the specification of the MINIMAL language, based on the text in `v37.min`.

###

arm-reference-manuall.pdf is the programmer's reference manual for the arm architecture.

###

x64-reference-manual.pdf is the reference manual for the x86 architecture.

###

nasm-reference-manual.pdf is the reference manual for the Netwide assembler used to build SPITBOL for Linux.

## Licensing

SPITBOL is licensed using the GPLv2 (or later) license.

[COPYING](COPYING) contains a copy of the GPLv2 license.

[COPYING-SAVE-FILE](COPYING-SAVE-FILES) describes licensing issues for a
SPITBOL "save file."

[COPYING-LOAD-MODULES](COPYING-LOAD-MODULES) describes licensing issues for a
SPITBOL "load module."

